function Raw_id_Labes(){
    
    $('.vManyToManyRawIdAdminField').prop('readonly',true);
    $('.vManyToManyRawIdAdminField').css('float','left');
    $('.vManyToManyRawIdAdminField').hide();
    $('.vManyToManyRawIdAdminField').each(function(){
      var field_id = $(this)
      if ($(this).val() != '') {
        var items = $(this).val().split(',');
        
        for (i=0;i<items.length;i++)
        {
            
            $.ajax({
                url: $(this).next('.related-lookup').attr('href') + items[i] + "/",
                cache: false,
                data:{ modelField_id:field_id.attr('id')},
                success:function(data){
                    
                    chosenId = this.url.split('/')[4]
                    modelField_id=this.url.split('modelField_id=')[1].split('&')[0]
                    
                    html = '<div  id="raw_id_object_'+chosenId+'" class="raw_id_object" style="margin-right:5px;margin-bottom:5px;border-radius: 4px;-webkit-border-radius: 4px;-moz-border-radius: 4px;float:left;font-size:11px;border:1px solid #ccc;padding:0px 4px;background:#FFF;">'
                    html += $.trim($(data).find('.breadcrumb').find('.active').text())
                    html += '<a onclick="remove_raw_obj(\''+modelField_id+'\','+chosenId+')" style="cursor:pointer;padding:3px;color:#c00;font-weight:600;text-decoration:none;">X</a>'
                    html += '</div>'
                    field_id.parent().append(html)
                }
            });
        }
      }
    });
}

$(document).ready(function(){
    Raw_id_Labes();
});

function dismissRelatedLookupPopup(win, chosenId) {
    var name = windowname_to_id(win.name);
    var elem = document.getElementById(name);
    console.log("elem name :"+name);
    console.log("elem elem :"+elem);
    console.log("chosen :"+chosenId);
    if (elem.className.indexOf('vManyToManyRawIdAdminField') != -1 && elem.value) {
        elem.value += ',' + chosenId;
    } else {
        document.getElementById(name).value = chosenId;
    }
    win.close();
    try {
        $.ajax({
                url: $('#'+name).next('.related-lookup').attr('href') + chosenId + "/",
                cache: false,
                success:function(data){
                    
                    
                    html = '<div id="raw_id_object_'+chosenId+'" class="raw_id_object" style="margin-right:5px;margin-bottom:5px;border-radius: 4px;-webkit-border-radius: 4px;-moz-border-radius: 4px;float:left;font-size:11px;border:1px solid #ccc;padding:0px 4px;background:#FFF;">'
                    html += $.trim($(data).find('.breadcrumb').find('.active').text())
                    html += '<a onclick="remove_raw_obj(\''+elem.id+'\','+chosenId+')" rel="'+chosenId+'" style="cursor:pointer;padding:3px;color:#c00;font-weight:600;text-decoration:none;">X</a>'
                    html += '</div>'
                    $('#'+name).parent().append(html);
            }
        });
    } catch(e) {
        console.log(e);
    }

}

function remove_raw_obj(modelField_id,chosenId){
    
    $('#raw_id_object_'+chosenId).remove();
    
    var item_array = $('#'+modelField_id).val().split(',');
    var index = item_array.indexOf(chosenId.toString());
    if (index > -1) {
        item_array.splice(index, 1);
    }
    
    if(item_array == undefined || $('#'+modelField_id).val().indexOf(',') == -1){
        html = ''
    }else{
        html = ''
        for (i=0;i<item_array.length;i++)
        {
            if (html != '') {
                html += ','+item_array[i]
            }else{ html += item_array[i] }
            
        }
    }
    $('#'+modelField_id).val(html)
}

